import firebase from "firebase";

const firebaseConf = {
  apiKey: "AIzaSyBbUM7QajbjVRxcc_EiEfnSNvPEpn6fe9Q",
  authDomain: "wicked-app-ed728.firebaseapp.com",
  projectId: "wicked-app-ed728",
  storageBucket: "wicked-app-ed728.appspot.com",
  messagingSenderId: "253862591886",
  appId: "1:253862591886:web:5fe1802124c7ea8fd7f8df",
  measurementId: "G-DTF7J3W6K6",
  databaseURL: "https://wicked-app-ed728.firebaseio.com",
};

const firebaseClient = () => {
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConf);
  }
};

export default firebaseClient;
