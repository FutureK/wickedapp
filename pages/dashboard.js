import React from "react";
import Link from "next/link";
import Container from "../components/container";
import { Flex, Box, Heading, Text, Button } from "@chakra-ui/react";
import nookies from "nookies";
import { verifyIdToken } from "../firebaseAdmin";
import firebaseClient, { firestore } from "../firebaseClient";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const Dashboard = ({ session }) => {
  firebaseClient();
  console.log(session);

  const { Projects, name } = session;

  if (session) {
    return (
      <Container>
        <Flex>
          <Box w={500} mx={"auto"}>
            <Heading as="h2" textAlign="center">
              Welcome {name}
            </Heading>
            <Text>
              {Projects[0].name} {Projects[0].id}
            </Text>
          </Box>
          <Link href="/">
            <Button variant="solid" colorScheme="blue">
              <a>Back to home</a>
            </Button>
          </Link>
        </Flex>
      </Container>
    );
  } else {
    return (
      <Box>
        <Text>Loading </Text>
      </Box>
    );
  }
};

export default Dashboard;

export async function getServerSideProps(context) {
  try {
    const cookies = nookies.get(context);
    const token = await verifyIdToken(cookies.token);
    const firestore = firebase.firestore();
    const userRef = firestore.collection("Users").doc(token.user_id);
    const userData = (await userRef.get()).data();
    return {
      props: { session: userData }, // will be passed to the page component as props
    };
  } catch (err) {
    console.log(err);
    return { props: { error: "hi" } };
  }
}
